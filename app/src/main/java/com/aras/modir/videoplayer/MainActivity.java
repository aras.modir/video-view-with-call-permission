package com.aras.modir.videoplayer;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {
    VideoView videoView;
    String url = "https://hw20.cdn.asset.aparat.com/aparat-video/c65ef9f6b02fa7a19160ef1c147ebb8f11175144-480p__51991.mp4";
    BroadcastReceiver broadcastReceiver;
    public int getVideoCurrentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        checkPermissions();

        videoView.setMediaController(new MediaController(this));
        videoView.setVideoURI(Uri.parse(url));
        videoView.start();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (videoView.isPlaying()) {
                    videoView.pause();
                    getVideoCurrentPosition = videoView.getCurrentPosition();
                }
            }
        };

        IntentFilter callFilter = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(broadcastReceiver, callFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE}, 100);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void bind() {
        videoView = findViewById(R.id.myvideo);
    }

    @Override
    protected void onResume() {
        super.onResume();
            videoView.seekTo(getVideoCurrentPosition);
        videoView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getVideoCurrentPosition = videoView.getCurrentPosition();
        videoView.pause();
    }
}
